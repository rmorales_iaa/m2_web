Web interface for m2 and its databases using
    - Play framework 2.8
    - ReactiveMongo
    - Scala 2.13 as programming language
    - sbt 1.9.4 
    - Visual Code for compiling and debugging
