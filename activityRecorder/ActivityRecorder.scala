package activityRecorder
//=============================================================================
object ActivityRecorder {
  //=============================================================================
  private val filePath = "" // Define the path to the file
  //=============================================================================
  // Method to append a Stats instance to the file
  def appendToStatsFile(stats: Stats): Unit = {
    val file = new File(statsFilePath)
    val bw = new BufferedWriter(new FileWriter(file, true)) // 'true' appends to the file

    try {
      // Write the Stats instance as a line to the file
      bw.write(s"${stats.user}, ${stats.ip}, ${stats.action}, ${stats.file}\n")
    } finally {
      bw.close() // Close the BufferedWriter
    }
  }
}
//=============================================================================
case class ActivityRecorder(user: String, ip: String, action: String, file: String)
//=============================================================================
