package activityRecorder
//=============================================================================
import logger.MyLogger
import java.io.File
import java.io.BufferedWriter
import java.io.FileWriter
import scala.util.{Try, Success, Failure}
import controllers.ControllerCommon
//=============================================================================
object ActivityRecorder {
  //---------------------------------------------------------------------------
  private val BASE_PATH = "/home/rafa/proyecto/m2_web/activityRecorder/log/" 
  //---------------------------------------------------------------------------
  def append(user: String
             , remoteIP: String
             , action: String
             , parameter: String): Unit = 
    append(ActivityRecorder(user, remoteIP, action, parameter))
  //---------------------------------------------------------------------------
  def append(stats: ActivityRecorder): Unit = {
    synchronized {    
      val bw = new BufferedWriter(new FileWriter(new File(BASE_PATH + stats.user + ".csv"), true)) 

      val result = Try(bw.write(s"${ControllerCommon.getCurrentTimeStamp()} {${stats.user},${stats.remoteIP},${stats.action},${stats.parameter}}\n"))    
      result match {
        case Success(_) => 
        case Failure(exception) => MyLogger.error("Error appending on ActivityRecorder file")     
      }         
      bw.close()  //finally
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ActivityRecorder(user: String
                            , remoteIP: String
                            , action: String
                            , parameter: String) {
  //---------------------------------------------------------------------------
  append                            
  //---------------------------------------------------------------------------
  private def append() = ActivityRecorder.append(this)
  //---------------------------------------------------------------------------
}
//=============================================================================
