package api.astrometrySolverApi
//=============================================================================
import sys.process._
import play.api.Configuration
//=============================================================================
import logger.MyLogger
import controllers.ControllerCommon.VERBOSE
import activityRecorder.ActivityRecorder
import scala.jdk.CollectionConverters._
import java.nio.file.Paths
import java.nio.file.Files
//=============================================================================
case class AstrometrySolverApi(conf: Configuration) {
  //---------------------------------------------------------------------------

  private val SOLVER_DEPLOY_DIR  = conf.get[String]("fits.solver.m2.dir ")
  private val SOLVER_COMMAND     = conf.get[String]("fits.solver.m2.command")
  //---------------------------------------------------------------------------  
                    
  def m_2_solver(inputDir:String
                 , userName: String
                 , remoteIP: String): String = {

    val outputDir = inputDir + "_solver"                   
    val command = SOLVER_COMMAND
                  .replace("$1",s"'$inputDir'")
                  .replace("$2",s"'$outputDir'")

    ActivityRecorder(userName
                     , remoteIP                         
                     , SOLVER_COMMAND
                     , command)

    if (VERBOSE) MyLogger.info(command)    
    val processBuilder = Process(Seq("bash", "-c", s"cd $SOLVER_DEPLOY_DIR && $command"))       
    processBuilder.!!    

    //fin the abosolute path to the solved directory
    val directory = Paths.get(outputDir)
    val filesInDirectory = Files.list(directory).iterator().asScala   
    filesInDirectory.find(Files.isRegularFile(_)).get.toAbsolutePath.toString
  }
  //---------------------------------------------------------------------------  
  def solve(parentDir:String
            , userName: String
            , remoteIP: String): String = {

    m_2_solver(parentDir, userName, remoteIP)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstrometrySolverApi.scala
//=============================================================================