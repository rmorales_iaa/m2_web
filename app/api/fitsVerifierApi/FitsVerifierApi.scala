package api.fitsVerifierApi
//=============================================================================
import sys.process._
import play.api.Configuration
//=============================================================================
import logger.MyLogger
import controllers.ControllerCommon.VERBOSE
import activityRecorder.ActivityRecorder
//=============================================================================
case class FitsVerifierApi(conf: Configuration) {
  //---------------------------------------------------------------------------
  private val DEPLOY_DIR          = conf.get[String]("fits.verifier.deploy.dir")
  private val COMMAND_VERIFY_FILE = conf.get[String]("fits.verifier.verify.file")
  //---------------------------------------------------------------------------
  //fitsVerifier configuration and api commands
  //---------------------------------------------------------------------------
  def verifyFile(fitsFileName:String
                 , userName: String
                 , remoteIP: String): FitsVerifierResult = {

    val command = COMMAND_VERIFY_FILE
                       .replace("$1",s"'$fitsFileName'")

    ActivityRecorder(userName
                     , remoteIP                         
                     , COMMAND_VERIFY_FILE
                     , command)

    if (VERBOSE) MyLogger.info(command)    
    val processBuilder = Process(Seq("bash", "-c", s"cd $DEPLOY_DIR && $command"))       
    val result = processBuilder.!!    
    FitsVerifierResult(result)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsVerifierApi.scala
//=============================================================================