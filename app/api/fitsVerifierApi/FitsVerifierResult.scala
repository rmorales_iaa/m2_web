package api.fitsVerifierApi
//=============================================================================
//=============================================================================
object FitsVerifierResult {
  //---------------------------------------------------------------------------
  val filePathPattern     = """.*Verifying file:.*""".r
  val warningCountPattern = """.*Warning disconformities:\s+(\d+)""".r
  val fatalCountPattern   = """.*Fatal   disconformities:\s+(\d+)""".r
  //---------------------------------------------------------------------------
  def apply(log: String) : FitsVerifierResult = {
  
    val imageName = filePathPattern.findFirstIn(log).get.split(":").last.replaceAll("'","").trim
    val warningMatch = fatalCountPattern.findFirstIn(log)
    val warningCount = if (warningMatch.isEmpty) 0L else warningMatch.get.split(":")(1).trim.toLong
    
    if (log.contains("None disconformity detected")) {
      FitsVerifierResult(
         imageName
        , isValid = true
        , fatalErrorsCount = 0L
        , warningCount
        , log
      )
    }
    else  {
      val fatalMatch = fatalCountPattern.findFirstIn(log)
      val fatalCount = if (fatalMatch.isEmpty) 0L else fatalMatch.get.split(":")(1).trim.toLong

      FitsVerifierResult(
         imageName
        , isValid = fatalCount == 0
        , fatalCount
        , warningCount
        , log
      )
    }    
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class FitsVerifierResult(  
    imageName : String
  , isValid: Boolean
  , fatalErrorsCount: Long
  , warningsCount: Long
  , log: String
  ) {
  //---------------------------------------------------------------------------
  def toSeq() = Seq(
    imageName
    , isValid.toString()
    , fatalErrorsCount.toString()
    , warningsCount.toString()
    , log
  )
  //---------------------------------------------------------------------------
  } 
  //---------------------------------------------------------------------------
   

//=============================================================================
//End of file FitsVerifierResult.scala
//=============================================================================