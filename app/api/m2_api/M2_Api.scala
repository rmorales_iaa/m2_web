package api.m2_api
//=============================================================================
import sys.process._
import play.api.Configuration
//=============================================================================
import logger.MyLogger
import controllers.ControllerCommon.VERBOSE
import activityRecorder.ActivityRecorder
import controllers.ControllerCommon
//=============================================================================
object M2_Api {                  
  //---------------------------------------------------------------------------
  private val M2_FINAL_RESULT_PREFIX_MPO_COMPOSED_NAME = "mpo composed name"
    //---------------------------------------------------------------------------
}
//=============================================================================
import M2_Api._
case class M2_Api(conf: Configuration) {
  //-------------------------------------------------------------------------
  private val M2_DEPLOY_DIR          = conf.get[String]("m2.deploy.dir")
  private val WEB_COMMAND_ASTROMETRY = conf.get[String]("m2.web.command.astrometry")
  private val WEB_COMMAND_EXISTS     = conf.get[String]("m2.web.command.exists")
  private val WEB_COMMAND_VR         = conf.get[String]("m2.web.command.vr")
  //-------------------------------------------------------------------------
  //m2 configuration and api commands
  //-------------------------------------------------------------------------
  def astrometry(spkID:String
                 , userName: String
                 , remoteIP: String
                 , m2_path: M2_Path
                 ): Option[M2_WebAstrometryResult] = {
                             
    val outputDir  = m2_path.M2_RESULT_M2_WEB_PATH
    val m2_command = WEB_COMMAND_ASTROMETRY
                       .replace("$1",s"'$spkID'")
                       .replace("$2",outputDir)

    ActivityRecorder.append(userName, remoteIP, WEB_COMMAND_VR, m2_command)

    if (VERBOSE) MyLogger.info(m2_command)    
    val processBuilder = Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))       
    val result = processBuilder.!!
    if (VERBOSE) MyLogger.info(result)

    if (result.contains("Unknown object")) None
    else Some(M2_WebAstrometryResult(outputDir, remoteIP, userName))
  }

  //-------------------------------------------------------------------------
  def exists(conf: Configuration
              , mpoName:String
              , userName: String
              , remoteIP: String
              ): Option[String] = {
                             
    val m2_command = WEB_COMMAND_EXISTS
                       .replace("$1",s"'$mpoName'")

    if (VERBOSE) MyLogger.info(m2_command)    

    ActivityRecorder.append(userName, remoteIP, WEB_COMMAND_EXISTS, m2_command)

    val processBuilder = Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))       
    val result = processBuilder.!!
    if (VERBOSE) MyLogger.info(result)
    if(result.contains("Unknown object")||
       !result.contains(M2_FINAL_RESULT_PREFIX_MPO_COMPOSED_NAME)) None
    else {   
      val mpoComposedName = 
        result
         .split("\n")
         .filter( _.contains(M2_FINAL_RESULT_PREFIX_MPO_COMPOSED_NAME))
         .head
         .split(":")
         .last
         .replaceAll("'","")
         .trim
      
      //get the V-R
      val m2_path = M2_Path(conf,mpoComposedName)
      ControllerCommon.resetDirectory(m2_path.M2_RESULT_M2_WEB_PATH)
      vr(mpoName, userName, remoteIP, m2_path)

      Some(mpoComposedName)
    }    
  }
  //-------------------------------------------------------------------------
  def vr(spkID: String
       , userName: String
       , remoteIP: String
       , m2_path: M2_Path
      ): Option[String] = {
    
    if (m2_path == null) return None
                           
    val outputDir = m2_path.M2_RESULT_M2_WEB_PATH
    val outputFileName = s"$outputDir/vr.csv"
    if (ControllerCommon.fileExist(outputFileName)) Some(outputFileName)
    else {
       val m2_command = WEB_COMMAND_VR
                       .replace("$1",s"'$spkID'")
                       .replace("$2",outputDir)
       ActivityRecorder.append(userName, remoteIP, WEB_COMMAND_VR, m2_command)
       if (VERBOSE) MyLogger.info(m2_command)    
       val processBuilder = Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))       
       val result = processBuilder.!!
       if (VERBOSE) MyLogger.info(result)
       if (result.contains("No results for")) None
       else Some(outputFileName)
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file M2_Api.scala
//=============================================================================