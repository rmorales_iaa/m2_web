package api.m2_api
//=============================================================================
import play.api.Configuration
//=============================================================================
import controllers.ControllerCommon.findFirstSubDirContains
import controllers.ControllerCommon.ensureDirectoryExist
//=============================================================================
//=============================================================================
object M2_Path{
  //---------------------------------------------------------------------------
  val M2_RESULT_PHOT_ABSO_PATH="light_curve/all_image_filters/by_catalog_filter/GAIA_DR3/JOHNSON_R"
  //---------------------------------------------------------------------------
  def getM2_WebPath(conf: Configuration, mpoComposedName: String) = {
    val dir = conf.get[String]("m2.web.temporal.dir") + s"/$mpoComposedName/"
    ensureDirectoryExist(dir)
    dir 
  }   
  //---------------------------------------------------------------------------
}
//=============================================================================
import M2_Path._
case class M2_Path(conf: Configuration
                  , mpoComposedName:String) {

  //-------------------------------------------------------------------------                    
  private val mpcID = if (mpoComposedName.startsWith("mpo_")) mpoComposedName.split("_")(1) else "none"              
  //-------------------------------------------------------------------------                    
  private def getResultRootDirectory() =   
    findFirstSubDirContains(conf.get[String]("m2.results.root.path")
                           , mpcID).get + "/"    
  //-------------------------------------------------------------------------
  private val M2_RESULT_ROOT_PATH         = if (mpcID == "none") "" else s"${getResultRootDirectory()}"

  val M2_RESULT_PHOT_ABSO_COMPRESS_PATH   = s"$M2_RESULT_ROOT_PATH/2_photometry/absolute"
  val M2_RESULT_PHOT_ABSO_ROT_PHASE_PATH  = s"$M2_RESULT_PHOT_ABSO_COMPRESS_PATH/light_curve/all_image_filters/by_catalog_filter/GAIA_DR3/JOHNSON_R/rotational_period/best_period_0"

  val M2_RESULT_PHOT_DIFF_COMPRESS_PATH   = s"$M2_RESULT_ROOT_PATH/2_photometry/differential/rotational_period/merge/GAIA_DR3/JOHNSON_R/best_period_0"
  val M2_RESULT_PHOT_DIFF_ROT_PHASE_PATH  = s"$M2_RESULT_PHOT_DIFF_COMPRESS_PATH/"

  val M2_RESULT_COMPILED_COMPRESS_PATH    = s"$M2_RESULT_ROOT_PATH/3_compiled_results"

  val M2_RESULT_M2_WEB_PATH               = getM2_WebPath(conf,mpoComposedName)

  private val M2_RESULT_PHOT_ABSO_GNUPLOT_ROOT_PATH = s"$M2_RESULT_PHOT_ABSO_COMPRESS_PATH/$M2_RESULT_PHOT_ABSO_PATH"

  val M2_RESULT_PHOT_ABSO_GNUPLOT_SCRIPT_NAME_SEQ = Seq(
      (s"$M2_RESULT_PHOT_ABSO_GNUPLOT_ROOT_PATH/plot_jd_est_absolute_magnitude.gnuplot",false)
    , (s"$M2_RESULT_PHOT_ABSO_GNUPLOT_ROOT_PATH/plot_jd_est_magnitude.gnuplot",false)
    , (s"$M2_RESULT_PHOT_ABSO_GNUPLOT_ROOT_PATH/plot_jd_est_reduced_magnitude.gnuplot",false)
    , (s"$M2_RESULT_PHOT_ABSO_GNUPLOT_ROOT_PATH/plot_phase_angle_reduced_magnitude.gnuplot",false)
    , (s"$M2_RESULT_PHOT_ABSO_ROT_PHASE_PATH/estimated_magnitude.gnuplot",true)
  )

  val M2_RESULT_PHOT_DIFF_GNUPLOT_SCRIPT_NAME_SEQ = Seq(
      (s"$M2_RESULT_PHOT_DIFF_ROT_PHASE_PATH/estimated_magnitude.gnuplot",true)
  )
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file M2_Path.scala
//=============================================================================