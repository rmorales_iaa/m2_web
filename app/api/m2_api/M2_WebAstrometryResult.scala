package api.m2_api
//=============================================================================
import java.io.{File}
import java.time.LocalDateTime
import org.apache.commons.csv.{CSVFormat, CSVParser, CSVRecord}
import scala.io.Source
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.mutable.ListBuffer
import scala.sys.process._
import java.time.format.DateTimeFormatter
import scala.util.{Failure, Success, Try}
//=============================================================================
import logger.MyLogger
import forms.MpoInfoForm
import controllers.ControllerCommon._
//=============================================================================
//=============================================================================
object M2_WebAstrometryResult {
  //---------------------------------------------------------------------------
  private val localDateTimeFormatterWithMillis = DateTimeFormatter.ofPattern("yyyy_MM_dd'T'HH_mm_ss_SSS")
  //---------------------------------------------------------------------------
  final val CSV_HEADER = Seq(
      "fileName"
    , "raMin"	
    , "raMax"	
    , "decMin"	
    , "decMax"	
    , "fovX"	
    , "fovY"	
    , "pixScaleX"	
    , "pixScaleY"
    , "telescope"
    , "filter"
    , "instrument"
    , "exposure_time"
  ).mkString(CSV_DIVIDER) 
  //---------------------------------------------------------------------------
  private def resetStatusValueMap() = 
    Map(
        "list_telescope"            -> ANY_USER_VALUE
      , "list_image_filter"         -> ANY_USER_VALUE
      , "list_instrument"           -> ANY_USER_VALUE
      , "list_exp_time_s"           -> ANY_USER_VALUE
      , "list_telescope"            -> ANY_USER_VALUE
      , "input_custom_obs_min_time" -> ANY_USER_VALUE
      , "input_custom_obs_max_time" -> ANY_USER_VALUE
    )  
  //---------------------------------------------------------------------------
  private def updateStatusValueMap(userFilterData: MpoInfoForm.UserData) = 
    Map(
        "list_telescope"            -> userFilterData.telescope
      , "list_image_filter"         -> userFilterData.filter
      , "list_instrument"           -> userFilterData.instrument
      , "list_exp_time_s"           -> userFilterData.expTime
      , "input_custom_obs_min_time" -> userFilterData.customObsMinTime
      , "input_custom_obs_max_time" -> userFilterData.customObsMaxTime
    )
  //---------------------------------------------------------------------------
  private def loadInfoFile(infoFileName: String): Map[String,String] = {
    Source.fromFile(infoFileName).getLines().flatMap { line =>
    if (line.trim.startsWith("#")) None
    else {      
      val parts = line.split("\t").map(_.trim.replaceAll("'",""))    
      if (parts.length == 2) Some(parts(0).trim -> parts(1).trim)
      else None
      }
    }.toMap
  }
  //---------------------------------------------------------------------------
  private def loadCsvFile(csvFile: String): Seq[CSVRecord] = {

    // Define the CSV format
    val csvFormat = CSVFormat
      .DEFAULT
      .withHeader()
      .withDelimiter('\t')// Assumes the first row is the header

    // Open the CSV file and parse it
    val csvParser = CSVParser.parse(new java.io.FileReader(csvFile), csvFormat)
    val rowSeq = csvParser.getRecords().asScala
    csvParser.close()

    rowSeq.toSeq.sortBy { csvRecord => parseTimeStamp(csvRecord.get("fileName").trim)}.reverse
  }  
  //---------------------------------------------------------------------------
  private def parseLocalDateTime(date: String, timeFormat: DateTimeFormatter) = {
    var localDateTime: LocalDateTime = null
    Try { localDateTime = LocalDateTime.parse(date,timeFormat) }
    match {
      case Success(_) => Some(localDateTime)
      case Failure(_) => None
    }
  }
  //---------------------------------------------------------------------------
  //(timeStamp, telescope, filter, xPixSize, yPixSize, exposureTime, originalImageName, instrument, objectName)
  private  def getMetadaFromImageName(imageName: String) =  {
    val itemSeq = getFileNameNoPathNoExtension(imageName).split("#")
    val timeStamp = parseLocalDateTime(itemSeq(1), localDateTimeFormatterWithMillis).get

    val pixSizeItem   = itemSeq(2).split("x")
    val filter    = itemSeq(3)
    val telescope = itemSeq(4)
    val exposureTime = itemSeq(5).replaceAll("s","").toDouble
    val (originalImageName,instrument,objectName) =
      if (itemSeq == 8) (itemSeq(6), itemSeq(7), itemSeq(8))
      else ("UNKNOWN","UNKNOWN","UNKNOWN")
    (timeStamp
      , telescope
      , filter
      , pixSizeItem.head.toInt  //pix size x
      , pixSizeItem.last.toInt  //pix size y
      , exposureTime
      , originalImageName
      , instrument
      , objectName
    )
  }
  //---------------------------------------------------------------------------
  private def updateSelectSeq(csvRowSeq: Seq[CSVRecord]) = {
    val telescopeSet   = scala.collection.mutable.Set[String](ANY_USER_VALUE)
    val imageFilterSet = scala.collection.mutable.Set[String](ANY_USER_VALUE)
    val instrumentSet  = scala.collection.mutable.Set[String](ANY_USER_VALUE)
    val expTimeSet     = scala.collection.mutable.Set[String]()
    val obsTimeSet     = scala.collection.mutable.Set[LocalDateTime]()

    csvRowSeq.foreach { record =>
        val fileName = record.get("fileName").trim

        val (timeStamp
      , telescope
      , filter
      , pixSizeX
      , pixSizeY
      , exposureTime
      , originalImageName
      , instrument
      , objectName) = getMetadaFromImageName(fileName)

        telescopeSet   += telescope
        imageFilterSet += filter
        instrumentSet  += instrument
        expTimeSet     += exposureTime.toString()
        obsTimeSet     += timeStamp
    }
    
    val sortedObsTime = obsTimeSet.toList.sortWith(_ .isBefore(_))
    val (minObsTime,maxObsTime) =
      if (sortedObsTime.isEmpty) (ANY_USER_VALUE,ANY_USER_VALUE)
      else {
        (sortedObsTime.head.format(dateTimeFormatterWithMillis)
       , sortedObsTime.last.format(dateTimeFormatterWithMillis))
     }
    
    (telescopeSet.toSeq.sorted
     , imageFilterSet.toSeq.sorted
     , instrumentSet.toSeq.sorted
     , ANY_USER_VALUE +: expTimeSet.toSeq.sortWith( _.toDouble.round > _.toDouble.round)
     , obsTimeSet.toSeq
     , sortedObsTime
     , minObsTime
     , maxObsTime)
  }
  //---------------------------------------------------------------------------
  def apply(resultDir: String
            , remoteIP: String
            , userName: String): M2_WebAstrometryResult = {
    val fileSeq = new File(resultDir).listFiles()
    val csvFileImageList = fileSeq.find(p=> p.getAbsolutePath.endsWith("image_list.csv")).head.getAbsolutePath()
    val kernelReportFile = fileSeq.find(p=> p.getAbsolutePath().endsWith(".info")).head.getAbsolutePath()    


    if (VERBOSE) MyLogger.info(s"CSV file:  '$csvFileImageList'")
    if (VERBOSE) MyLogger.info(s"info file: '$kernelReportFile'")

    val csvRowSeq = loadCsvFile(csvFileImageList)
    val infoMap   = loadInfoFile(kernelReportFile)

    if (VERBOSE) MyLogger.info(s"CSV file map items : '$csvFileImageList'  -> '${infoMap.size}'")
    if (VERBOSE) MyLogger.info(s"info file row count: '$kernelReportFile' -> '${csvRowSeq.size}'")

    val (telescopeSet
        , imageFilterSet
        , instrumentSet
        , expTimeSet
        , obsTimeSet
        , sortedObsTime
        , minObsTime
        , maxObsTime) = updateSelectSeq(csvRowSeq)

    val orbitSeq = infoMap("orbit seq")
      .replace("{","")
      .replace("}","")
      .split(",")
      .toSeq

    M2_WebAstrometryResult(
        infoMap("main designation")
      , infoMap("alternative designation")
      , infoMap("spkID")
      , infoMap("mpcID")
      , infoMap("image count")
      , orbitSeq
      , telescopeSet
      , imageFilterSet
      , instrumentSet
      , expTimeSet
      , minObsTime
      , maxObsTime
      , csvRowSeq
      , ListBuffer.fill(csvRowSeq.size)(true)
      , filteredRowCount = csvRowSeq.size
      , statusValueMap = resetStatusValueMap()
      , remoteIP
      , userName
    )
  }
  //---------------------------------------------------------------------------
  def apply(): M2_WebAstrometryResult = 
    M2_WebAstrometryResult(
      name             = ""
    , altName          = ""
    , spkID            = ""
    , mpcID            = ""
    , imageCount       = ""
    , orbitSeq         = Seq()
    , telescopeSet     = Seq()
    , imageFilterSet   = Seq()
    , instrumentSet    = Seq()
    , expTimeSet       = Seq()
    , minObsTime       = ""
    , maxObsTime       = ""
    , rowSeq           = Seq[CSVRecord]()
    , viewRowSeq       = ListBuffer[Boolean]()
    , statusValueMap   = resetStatusValueMap()
    , remoteIP         = ""
    , userName         = ""
  )
  //---------------------------------------------------------------------------
}
//=================================================defaultValuseSeq============================
import M2_WebAstrometryResult._
case class M2_WebAstrometryResult(  
  name:                   String
  , altName:              String
  , spkID:                String
  , mpcID:                String
  , imageCount:           String  
  , orbitSeq:             Seq[String]
  , var telescopeSet:     Seq[String]
  , var imageFilterSet:   Seq[String]
  , var instrumentSet:    Seq[String]
  , var expTimeSet:       Seq[String]
  , var minObsTime:       String
  , var maxObsTime:       String
  , rowSeq:               Seq[CSVRecord]
  , var viewRowSeq:       scala.collection.mutable.ListBuffer[Boolean]
  , var filteredRowCount: Long = 0
  , var statusValueMap:   Map[String,String]
  , val remoteIP:         String
  , val userName:         String
  ) {
  //---------------------------------------------------------------------------
  val composedName = s"mpo_${spkID}_${name}"  
  var genericParameter = ""
  //---------------------------------------------------------------------------
  def applyFilter(userFilterData: MpoInfoForm.UserData): Unit = {
    //-------------------------------------------------------------------------
    val filterByTelescope  = userFilterData.telescope.toLowerCase.trim()
    val filterByFilter     = userFilterData.filter.toLowerCase.trim()
    val filterByInstrument = userFilterData.instrument.toLowerCase.trim()
    val filterByExpTime    = userFilterData.expTime.trim()
    val filterByObsMinTime = userFilterData.customObsMinTime.toLowerCase.trim()
    val filterByObsMaxTime = userFilterData.customObsMaxTime.toLowerCase.trim()
    //-------------------------------------------------------------------------
    statusValueMap = updateStatusValueMap(userFilterData)
    //-------------------------------------------------------------------------
    def passFilterByTelescope(record: CSVRecord) = {
      if (filterByTelescope == ANY_USER_VALUE.toLowerCase()) true
      else record.get("telescope").toLowerCase.trim == filterByTelescope      
    }          
    //-------------------------------------------------------------------------
    def passFilterByFilter(record: CSVRecord) =  {
      if (filterByFilter == ANY_USER_VALUE.toLowerCase()) true
      else record.get("filter").toLowerCase.trim == filterByFilter      
    }
    //-------------------------------------------------------------------------
    def passFilterByInstrument(record: CSVRecord) = {
      if (filterByInstrument == ANY_USER_VALUE.toLowerCase()) true
      else record.get("instrument").toLowerCase.trim == filterByInstrument      
    }
    //-------------------------------------------------------------------------
    def passFilterByExpTime(record: CSVRecord) = 
      if (filterByExpTime == ANY_USER_VALUE ||
        filterByExpTime == ANY_USER_VALUE.toLowerCase) true
      else          
        record.get("exposure_time").trim.toDouble.toLong >= filterByExpTime.trim.toLong    

    //-------------------------------------------------------------------------
    def passFilterByObsTime(record: CSVRecord
                            , inputTimeStamp:String
                            , isUserObsMinTime: Boolean): Boolean = {
      if (inputTimeStamp.isEmpty() ||
          inputTimeStamp == ANY_USER_VALUE ||
          inputTimeStamp == ANY_USER_VALUE.toLowerCase) true 
      else {       
       val rowTimeStamp  = parseTimeStamp(record.get("observing_time").trim).getOrElse(return true)   
       val userTimeStamp = parseTimeStamp(inputTimeStamp).getOrElse(return true)       
       if (userTimeStamp.isEqual(rowTimeStamp)) true
       else {         
         if (isUserObsMinTime) rowTimeStamp.isAfter(userTimeStamp)
         else rowTimeStamp.isBefore(userTimeStamp)
       }
      }                        
    }    
    //-------------------------------------------------------------------------
    def passFilterByObsMinTime(record: CSVRecord) = 
      passFilterByObsTime(record, filterByObsMinTime, isUserObsMinTime = true)
    //-------------------------------------------------------------------------
    def passFilterByObsMaxTime(record: CSVRecord) = 
      passFilterByObsTime(record, filterByObsMaxTime, isUserObsMinTime = false)
    //-------------------------------------------------------------------------

    //check is at least one filter is applied
    if (filterByTelescope  == ANY_USER_VALUE && 
        filterByFilter     == ANY_USER_VALUE &&      
        filterByInstrument == ANY_USER_VALUE &&
        filterByExpTime    == ANY_USER_VALUE &&
        filterByObsMinTime == ANY_USER_VALUE &&
        filterByObsMaxTime == ANY_USER_VALUE
      ) return

     filteredRowCount = 0
     rowSeq.zipWithIndex.foreach { case (record,i) =>
       val rowPassFilter =
         passFilterByTelescope(record)  &&
         passFilterByFilter(record)     && 
         passFilterByInstrument(record) &&
         passFilterByExpTime(record)    &&
         passFilterByObsMinTime(record) &&    
         passFilterByObsMaxTime(record)     
       viewRowSeq(i) = rowPassFilter
       if (rowPassFilter) filteredRowCount += 1
    }
    updateSelectSeqAfterFilter()
  }
  //---------------------------------------------------------------------------
  private def getFilteredRow() = 
    (rowSeq zip viewRowSeq).flatMap { case (row,viewRow) =>
      if (viewRow) Some(row)
      else None 
    }  
  //---------------------------------------------------------------------------
  def showAllRowSeq() =  
     viewRowSeq = ListBuffer.fill(viewRowSeq.size)(true)
  //---------------------------------------------------------------------------
  private def updateSelectSeqAfterFilter() = {
     val (_telescopeSet
          , _imageFilterSet
          , _instrumentSet
          , _expTimeSet
          , _obsTimeSet
          , _sortedObsTime
          , _minObsTime
          , _maxObsTime) = updateSelectSeq(getFilteredRow())

    telescopeSet   = _telescopeSet
    imageFilterSet = _imageFilterSet
    instrumentSet  = _instrumentSet
    expTimeSet     = _expTimeSet
    minObsTime     = _minObsTime
    maxObsTime     = _maxObsTime
  }
  //---------------------------------------------------------------------------
  def reset() = {
    showAllRowSeq()    
    statusValueMap  = resetStatusValueMap()
    updateSelectSeqAfterFilter()
    filteredRowCount = rowSeq.size
  }
  //---------------------------------------------------------------------------
  def getCsvContent() = {
    (rowSeq zip viewRowSeq).flatMap { case (row,viewRow) =>
      if (viewRow) 
        Some(row.toList.toArray.mkString(CSV_DIVIDER))      
      else None 
    }
  }
  //---------------------------------------------------------------------------
  def downladAndCompressFileSeq() = {
    //prepare directories
    val baseDir = generateUniqueDirectoryName(s"$TMP_DIR/${userName}_${remoteIP}_")
    val imageDirName= s"$baseDir/image_seq"
    val compressedDirName = s"$baseDir/image_compressed"
    val imageDir = new File(imageDirName)
    val compressedDir = new File(compressedDirName)

    imageDir.mkdirs()
    compressedDir.mkdirs()

    if (VERBOSE) {
      MyLogger.info(s"downladAndCompressFileSeq.imageDir: '$imageDir'")
      MyLogger.info(s"downladAndCompressFileSeq.compressedDir: '$compressedDir'")
      MyLogger.info(s"Copying remote files from 'nakbe' to '$imageDirName'")
    }

    //copy remote files    
    (rowSeq zip viewRowSeq).foreach{ case (row,viewRow) =>
      if (viewRow) {
      val command = s"scp nakbe:${row.get("fileName").trim} $imageDirName"
      command.!
      }
    }

    if (VERBOSE) MyLogger.info(s"Compressing: '$imageDirName' into $compressedDirName/image_seq.zip")
    val compressedFile = compressDirectoryWithZip(imageDirName, s"$compressedDirName/image_seq.zip")

    if (VERBOSE) MyLogger.info(s"Deleting directory: '$imageDirName'")

    //delete firectory
    new File(imageDirName).delete()

    compressedFile
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file M2_WebAstrometryResult.scala
//=============================================================================