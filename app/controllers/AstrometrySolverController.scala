/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
 */
//=============================================================================
package controllers
//=============================================================================
import java.io.File
import javax.inject._
import akka.stream.IOResult
import akka.stream.scaladsl._
import akka.util.ByteString
import play.api._
import play.api.libs.streams._
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc._
import play.core.parsers.Multipart.FileInfo
import scala.concurrent.{ExecutionContext}
import java.nio.file.attribute.PosixFilePermissions
import java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import java.nio.file.Files
import java.nio.file.Paths
//=============================================================================
import forms.AstrometrySolverForm._
import version.Version.M2_VERSION
import api.astrometrySolverApi.AstrometrySolverApi
import logger.MyLogger
import _root_.controllers.ControllerCommon.VERBOSE
//=============================================================================
object AstrometrySolverController {
  //---------------------------------------------------------------------------
  //temporal files
  private val TEMP_FILE_PREFIX      = "solver_"
  private val TEMP_FILE_SUFFIX      = ".fits"
  private val TEMP_FILE_PERMISSIONS = java.util.EnumSet.of(OWNER_READ, OWNER_WRITE)
  private val TEMP_FILE_ATTRIBUTE   = PosixFilePermissions.asFileAttribute(TEMP_FILE_PERMISSIONS)
  //---------------------------------------------------------------------------
}
//=============================================================================
import AstrometrySolverController._
class AstrometrySolverController @Inject()
  (cc: MessagesControllerComponents
  , conf: Configuration) 
  (implicit val executionContext: ExecutionContext)
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private var userName = ""
  //---------------------------------------------------------------------------
  private val astrometrySolverApi =  AstrometrySolverApi(conf)    
  //---------------------------------------------------------------------------
  private val TEMP_FILE_DIR = conf.get[String]("fits.solver.temporal.dir") 
  //---------------------------------------------------------------------------
  type FilePartHandler[A] = FileInfo => Accumulator[ByteString, FilePart[A]]
  //---------------------------------------------------------------------------
  def index(_userName:String) = Action { implicit request: MessagesRequest[AnyContent] =>   
    UserLoginController.withValidatedUser { userRequest => 
      userName = _userName
      if (VERBOSE) MyLogger.info(s"AstrometrySolverController.index")       
      Ok(views.html.astrometrySolver(form,M2_VERSION))
    }      
  }
  //---------------------------------------------------------------------------
  def upload = Action(parse.multipartFormData(handleFilePartAsFile)) {implicit request =>
  if (VERBOSE) MyLogger.info(s"AstrometrySolverController.upload")  
  request.body.file("path").map {
    case FilePart(key, filename, contentType, file, fileSize, dispositionType, refToBytes) =>
      MyLogger.info(s"key = $key, filename = $filename, contentType = $contentType, file = $file, fileSize = $fileSize, dispositionType = $dispositionType")      
      val solvedPath = astrometrySolverApi.solve(file.getParent
                                                 , userName
                                                 , remoteIP = request.remoteAddress.toString())      
      Files.deleteIfExists(file.toPath)

      ControllerCommon.downloadFile(solvedPath
                                   , ControllerCommon.removeFileExtension(filename) + "_solved.fits"
                                   , "fits")      
     }.getOrElse {
     Ok(views.html.astrometrySolver(form,M2_VERSION)).flashing("error" -> "Missing file")
   }    
  }        
  //---------------------------------------------------------------------------
  //https://www.playframework.com/documentation/2.8.x/ScalaFileUpload
  private def handleFilePartAsFile: FilePartHandler[File] = {
    case FileInfo(partName, filename, contentType, dispositionType) =>
      val tmpDir = Paths.get(ControllerCommon.generateUniqueDirectoryName(TEMP_FILE_DIR))
      val path        = Files.createTempFile(tmpDir
                                             , TEMP_FILE_PREFIX
                                             , TEMP_FILE_SUFFIX
                                             , TEMP_FILE_ATTRIBUTE)
      val file        = path.toFile
      val fileSink    = FileIO.toPath(path)
      val accumulator = Accumulator(fileSink)
      if (ControllerCommon.VERBOSE) MyLogger.info(s"uploaded file: '${file.getAbsolutePath()}'")  
      accumulator.map {
        case IOResult(count, status) =>
          MyLogger.info(s"count = $count, status = $status")
          FilePart(partName, filename, contentType, file, count, dispositionType)
      }(executionContext)
   }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstrometrySolverController.scala
//=============================================================================