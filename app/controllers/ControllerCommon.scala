/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description:   
 */
//=============================================================================
package controllers
//=============================================================================
import akka.stream.scaladsl.Source
import akka.util.ByteString
import akka.stream.scaladsl.FileIO
import java.nio.file.Files
import java.util.zip.{ZipEntry, ZipOutputStream}
import java.io.{File,FileOutputStream,FileInputStream,PrintWriter}
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.nio.file.Paths
import play.api.mvc._
import play.api.mvc.ResponseHeader
import play.api.http.HttpEntity
import play.api.http.Status.OK
import play.api.http.HeaderNames.CONTENT_DISPOSITION
import scala.util.{Failure, Success, Try}
import scala.collection.mutable.ArrayBuffer
import java.util.{UUID}
import sys.process._
//=============================================================================
import logger.MyLogger
//=============================================================================
//=============================================================================
object ControllerCommon {
  //---------------------------------------------------------------------------
  final val VERBOSE = false
  //---------------------------------------------------------------------------
  val TMP_DIR = Paths.get("").toAbsolutePath.toString + "/tmp/"
  //---------------------------------------------------------------------------
  final val ANY_USER_VALUE = "ANY"
  //---------------------------------------------------------------------------
  final val CSV_DIVIDER = "\t"
 //---------------------------------------------------------------------------
  private val localFileSystemPrefix = "file://"
  //---------------------------------------------------------------------------
  private val ExtensionPattern = """\.[A-Za-z0-9]+$""".r
  //---------------------------------------------------------------------------
  val currentDirectory = new java.io.File(".").getCanonicalPath
  //---------------------------------------------------------------------------
  private final val ONLY_DATE_TIME_FORMATTER_SEQ = Seq(
      DateTimeFormatter.ofPattern("yyyy"),
      DateTimeFormatter.ofPattern("yyyy-MM"),
      DateTimeFormatter.ofPattern("yyyy-MM-dd"),
    )
  //---------------------------------------------------------------------------
  private final val DATE_TIME_FORMATTER_SEQ = Seq(
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH"),
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"),
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"),
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
    )
  //---------------------------------------------------------------------------  
  val dateTimeFormatterWithMillis = DATE_TIME_FORMATTER_SEQ.last   
  //---------------------------------------------------------------------------
  def parseTimeStamp(input: String): Option[LocalDateTime] = {
    var parsedTimeStamp: LocalDateTime = null

    if(!input.contains("T")) { //no time specification, only date
      ONLY_DATE_TIME_FORMATTER_SEQ.foreach { formatter =>
        Try {
          if (input.length == 4) {
            parsedTimeStamp = LocalDate.parse(input + "-01-01", formatter)
              .atTime(0, 0, 0)
          } else if (input.length == 7) {
            parsedTimeStamp = LocalDate.parse(input + "-01", formatter)
              .atTime(0, 0, 0)
          } else {
            parsedTimeStamp = LocalDate.parse(input, formatter)
              .atTime(0, 0, 0)
          }
        }
        match {
          case Success(_) => return Some(parsedTimeStamp)
          case Failure(ex) =>
        }
      }
    }
    else { //date ans time specification
      DATE_TIME_FORMATTER_SEQ.foreach { formatter =>
        Try {
          parsedTimeStamp = LocalDateTime.parse(input, formatter)
        }
        match {
          case Success(_) => return Some(parsedTimeStamp)
          case Failure(_: Exception) =>
        }
      }
    }
    None
  }
  //---------------------------------------------------------------------------
  def getCurrentTimeStamp(): LocalDateTime =     
    LocalDateTime.parse(
      LocalDateTime
        .now()
        .format(DATE_TIME_FORMATTER_SEQ.last)
      , DATE_TIME_FORMATTER_SEQ.last)   
  //-------------------------------------------------------------------------
  def directoryExist(s:String) : Boolean = {
    var newPath = s
    if (s.startsWith(localFileSystemPrefix))
      newPath = s.drop(localFileSystemPrefix.length)
    val f = new File(newPath)
    f.exists && f.isDirectory
  }      
    //-------------------------------------------------------------------------
  def fileExist(s:String) : Boolean = {
    var newPath = s
    if (s.startsWith(localFileSystemPrefix))
      newPath = s.drop(localFileSystemPrefix.length)
    val f = new File(newPath)
    f.exists && f.isFile()
  }   
  //-------------------------------------------------------------------------
  def getSubDirectoryList(dir: String): List[File] = {
    if (!directoryExist(dir)) return List[File]()
    new File(dir).listFiles.filter(_.isDirectory).toList
  }  
    //-------------------------------------------------------------------------
  def getOnlyFilename(s: String) : String = Paths.get(s).getFileName.toString
  //-------------------------------------------------------------------------
  def getFileExtension(s: String): String = ExtensionPattern findFirstIn s getOrElse ""
    //---------------------------------------------------------------------------
  def removeFileExtension(fileName: String) ={
    val path = Paths.get(fileName)
    if (path.getFileName.toString.lastIndexOf('.') > 0) 
      path.getFileName.toString.substring(0, path.getFileName.toString.lastIndexOf('.'))
    else  path.getFileName.toString
  } 
  //-------------------------------------------------------------------------
  def getFileNameNoPathNoExtension (s: String) : String = removeFileExtension(getOnlyFilename(s))
  //-------------------------------------------------------------------------
  def getSortedFileListRecursive(dir: String                               
                                 , sorted: Boolean = true
                                 , verbose: Boolean = false) = {
    //-----------------------------------------------------------------------
    val dirToProcess = ArrayBuffer[String]()
    val result = ArrayBuffer[File]()
    //-----------------------------------------------------------------------
    def processDir(d: String): Unit = {
      if (verbose) println(s"Parsing directory: '$d'")
      dirToProcess.remove(0)
      dirToProcess ++= (getSubDirectoryList(d) map (s=>s.getAbsolutePath))
      new File(d).listFiles.filter(_.isFile) map { f=> result += f }
    }
    //-----------------------------------------------------------------------
    dirToProcess += dir
    while (!dirToProcess.isEmpty) processDir(dirToProcess.head)
    if (sorted) result.sortWith( _.getAbsolutePath < _.getAbsolutePath) else result
  }   
  //-------------------------------------------------------------------------
  def ensureDirectoryExist(s:String) : Boolean = {
    val d = new java.io.File(s)
    if (!d.exists()) {
      if (!d.mkdirs()) {
        println(s"Error creating output directory:'$s'")
        return false
      }     
    }
    true
  }
  //-------------------------------------------------------------------------
  def deleteDirectoryIfExist(s: String): Unit =
    if (directoryExist(s)) s"rm -fr $s".!  
  //-------------------------------------------------------------------------
  def resetDirectory(s:String) : Boolean = {
    deleteDirectoryIfExist(s)
    ensureDirectoryExist(s)
  }
  //---------------------------------------------------------------------------
  def findFirstSubDirStartWith(rootDirectory: String
                             , s: String): Option[String] = {
    val root = new File(rootDirectory)

    if (root.exists() && root.isDirectory) {
      val subdirectories = root.listFiles(_.isDirectory)
      subdirectories.foreach { subdirectory =>
        if (subdirectory.getName.startsWith(s)) return Some(subdirectory.getAbsolutePath())
      }
    }     
    None    
  }
  //---------------------------------------------------------------------------
   def findFirstSubDirContains(rootDirectory: String
                          , s: String): Option[String] = {
    val root = new File(rootDirectory)

    if (root.exists() && root.isDirectory) {
      val subdirectories = root.listFiles(_.isDirectory)
      subdirectories.foreach { subdirectory =>
        if (subdirectory.getName.contains(s)) return Some(subdirectory.getAbsolutePath())
      }
    }     
    None    
  }
  //---------------------------------------------------------------------------
  def generateUniqueDirectoryName(prefix: String): String = {
    val uuid = UUID.randomUUID().toString.split("-").last
    val dirName = s"${prefix}rand-$uuid/"
    val dir = new File(dirName)
    if (dir.exists()) dir.delete()
    dir.mkdirs()
    dirName
  }
  //---------------------------------------------------------------------------
  def findCommonStartingPattern(strings: List[String]): String = {
    if (strings.isEmpty) ""
    else {
      val shortestString = strings.minBy(_.length)
      val maxLength = shortestString.length
      var commonPattern = ""
      
      for (i <- 0 until maxLength) {
        val currentChar = shortestString.charAt(i)
        val isCommon = strings.forall(s => s.charAt(i) == currentChar)
        
        if (isCommon) commonPattern += currentChar
        else  return commonPattern        
      }      
      commonPattern
    }
  }
  //---------------------------------------------------------------------------
  def compressDirectoryWithZip(inputDir: String
                               , zipFilename: String
                               , longBufferByteSize: Int = 1024 * 1024 * 100) = {
    val zipFile = new File(zipFilename)
    val zipOutput = new ZipOutputStream(new FileOutputStream(zipFile))
    val fileList = getSortedFileListRecursive(inputDir)
    if (!fileList.isEmpty) {
      val commonStartingPattern = Paths.get(
      findCommonStartingPattern((fileList map ( _.getAbsolutePath())).toList))
      .getParent.toString
    
      for (localFile <- getSortedFileListRecursive(inputDir)) {          
        val zipEntry = new ZipEntry(localFile.getAbsolutePath().replace(commonStartingPattern,""))
        zipOutput.putNextEntry(zipEntry)

        val inputStream = new FileInputStream(localFile)
        val buffer = new Array[Byte](longBufferByteSize)
        var bytesRead = inputStream.read(buffer)

        while (bytesRead != -1) {
          zipOutput.write(buffer, 0, bytesRead)
          bytesRead = inputStream.read(buffer)
        } 

        inputStream.close()
        zipOutput.closeEntry()
      }

      zipOutput.close()    
      zipFilename
    }
    else ""
  }
    //---------------------------------------------------------------------------
  def downloadFile(sourceFile: String
                  , downloadedFilename: String
                  , mimeExtension: String) =  {   
     val file                          = new java.io.File(sourceFile)
     val path                          = file.toPath
     val source: Source[ByteString, _] = FileIO.fromPath(path)
     val contentLength = Some(Files.size(path))

     if (VERBOSE) MyLogger.info(s"Downloading file '$sourceFile'")
     val headers = Map(
      CONTENT_DISPOSITION -> s"attachment; filename=$downloadedFilename"
    )

     Result(
       header = ResponseHeader(OK, headers)
       , body = HttpEntity.Streamed(source, contentLength, Some(s"application/$mimeExtension"))
     )   
  }
  //---------------------------------------------------------------------------
  def fixGnuPlotScript(inputFilePath: String
                   , outputFilePath: String) = {
    val inputLines = scala.io.Source.fromFile(inputFilePath).getLines().toList
    val csvFileName = Paths.get(inputFilePath).getParent.toString + "/light_curve.csv"
    val filteredLines = inputLines.flatMap{ line=> 
      if (line.startsWith("csv")) Some(s"csv='$csvFileName'")
      else 
        if (line.startsWith("pause -1")) None
        else Some(line)
    }
    
    val writer = new PrintWriter(outputFilePath)
    filteredLines.foreach(line => writer.println(line))
    writer.close()
  }
    //---------------------------------------------------------------------------
  def fixGnuRotPhaseScript(inputFilePath: String
                         , outputFilePath: String) = {
    val inputLines = scala.io.Source.fromFile(inputFilePath).getLines().toList
    val csvFileName = Paths.get(inputFilePath).getParent.toString + "/estimated_magnitude.csv"
    val filteredLines = inputLines.flatMap{ line=> 
      if (line.startsWith("my_data_file=")) Some(s"my_data_file='$csvFileName'")
      else 
        if (line.startsWith("pause -1")) None
        else 
          if (line.startsWith("set term x11 ")) None
          else Some(line)
    }
    
    val writer = new PrintWriter(outputFilePath)
    filteredLines.foreach(line => writer.println(line))
    writer.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ControllerCommon.scala
//=============================================================================