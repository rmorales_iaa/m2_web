package controllers
//=============================================================================
import javax.inject._
import play.api._
import play.api.mvc._
//=============================================================================
import version.Version.M2_VERSION
//=============================================================================
@Singleton
class ErrorMessage @Inject()
  (cc: MessagesControllerComponents
  , conf: Configuration) 
    extends MessagesAbstractController(cc) {

  //---------------------------------------------------------------------------    
  def index() = Action { implicit request: MessagesRequest[AnyContent] =>
     Ok(views.html.errorMessage("Please continue",M2_VERSION))  
  }
  //---------------------------------------------------------------------------
  def showError(message: String) = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.errorMessage(message,M2_VERSION))  
  }
  //---------------------------------------------------------------------------
}
//=============================================================================