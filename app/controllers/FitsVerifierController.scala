/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
 */
//=============================================================================
package controllers
//=============================================================================
import java.io.File
import javax.inject._
import akka.stream.IOResult
import akka.stream.scaladsl._
import akka.util.ByteString
import play.api._
import play.api.libs.streams._
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc._
import play.core.parsers.Multipart.FileInfo
import scala.concurrent.{ExecutionContext}
import java.nio.file.attribute.PosixFilePermissions
import java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import java.nio.file.Files
import java.nio.file.Paths
//=============================================================================
import forms.FitsVerifierForm._
import version.Version.M2_VERSION
import api.fitsVerifierApi.FitsVerifierApi
import logger.MyLogger
import api.fitsVerifierApi.FitsVerifierResult
import _root_.controllers.ControllerCommon.VERBOSE
//=============================================================================
object FitsVerifierController {
  //---------------------------------------------------------------------------
  //temporal files
  private val TEMP_FILE_PREFIX      = "fitsVerifier_"
  private val TEMP_FILE_SUFFIX      = ".fits"
  private val TEMP_FILE_PERMISSIONS = java.util.EnumSet.of(OWNER_READ, OWNER_WRITE)
  private val TEMP_FILE_ATTRIBUTE   = PosixFilePermissions.asFileAttribute(TEMP_FILE_PERMISSIONS)
  //---------------------------------------------------------------------------
}
//=============================================================================
import FitsVerifierController._
class FitsVerifierController @Inject()
  (cc: MessagesControllerComponents
  , conf: Configuration) 
  (implicit val executionContext: ExecutionContext)
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private var userName = ""
  //---------------------------------------------------------------------------
  private val fitsVerifyApi =  FitsVerifierApi(conf)    
  private var fitsVerifierResult: FitsVerifierResult = null
  //---------------------------------------------------------------------------
  private val TEMP_FILE_DIR = Paths.get(conf.get[String]("fits.verifier.temporal.dir") + "/")
  //---------------------------------------------------------------------------
  type FilePartHandler[A] = FileInfo => Accumulator[ByteString, FilePart[A]]
  //---------------------------------------------------------------------------
  def index(_userName:String) = Action { implicit request: MessagesRequest[AnyContent] =>     

    UserLoginController.withValidatedUser { userRequest => 
      userName = _userName
      if (VERBOSE) MyLogger.info(s"FitsVerifierController.index")       
      Ok(views.html.fitsVerifier(form,M2_VERSION))
    }
  }
  //---------------------------------------------------------------------------
  def upload = Action(parse.multipartFormData(handleFilePartAsFile)) {implicit request =>
    if (VERBOSE) MyLogger.info(s"FitsVerifierController.upload")  
    request.body.file("path").map {
      case FilePart(key, filename, contentType, file, fileSize, dispositionType, refToBytes) =>
        MyLogger.info(s"key = $key, filename = $filename, contentType = $contentType, file = $file, fileSize = $fileSize, dispositionType = $dispositionType")      
        fitsVerifierResult = fitsVerifyApi.verifyFile(
           fitsFileName = file.getAbsolutePath()
           , userName
           , remoteIP = request.remoteAddress.toString())
        Files.deleteIfExists(file.toPath)
        Redirect(routes.FitsVerifierController.showResult())           
       }.getOrElse {
       Ok(views.html.fitsVerifier(form,M2_VERSION)).flashing("error" -> "Missing file")
     }    
  }        
  //---------------------------------------------------------------------------
  def showResult() = Action { implicit request: MessagesRequest[AnyContent] =>         
    if (VERBOSE) MyLogger.info(s"FitsVerifierController.results")       
    Ok(views.html.fitsVerifierResult(fitsVerifierResult.toSeq(),M2_VERSION))
  }
  //---------------------------------------------------------------------------
  //https://www.playframework.com/documentation/2.8.x/ScalaFileUpload
  private def handleFilePartAsFile: FilePartHandler[File] = {
    case FileInfo(partName, filename, contentType, dispositionType) =>
      val path        = Files.createTempFile(TEMP_FILE_DIR
                                             , TEMP_FILE_PREFIX
                                             , TEMP_FILE_SUFFIX
                                             , TEMP_FILE_ATTRIBUTE)
      val file        = path.toFile
      val fileSink    = FileIO.toPath(path)
      val accumulator = Accumulator(fileSink)
      if (VERBOSE) MyLogger.info(s"uploaded file: '${file.getAbsolutePath()}'")  
      accumulator.map {
        case IOResult(count, status) =>
          MyLogger.info(s"count = $count, status = $status")
          FilePart(partName, filename, contentType, file, count, dispositionType)
      }(executionContext)
   }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsVerifierController.scala
//=============================================================================