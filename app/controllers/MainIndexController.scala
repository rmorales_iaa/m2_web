/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
 */
//=============================================================================
package controllers
//=============================================================================
import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.{ExecutionContext}
//=============================================================================
import version.Version.M2_VERSION
import logger.MyLogger
import _root_.controllers.helpers.ActionWithValidUser
//=============================================================================
class MainIndexController @Inject()
  (cc: MessagesControllerComponents
  , actionWithValidUser: ActionWithValidUser
  , conf: Configuration) 
  (implicit val executionContext: ExecutionContext)
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private var userName  = ""    
  //---------------------------------------------------------------------------
  def index(_userName: String) = Action { implicit request: MessagesRequest[AnyContent] =>     
    UserLoginController.withValidatedUser { userRequest => 
      userName = _userName
      if (ControllerCommon.VERBOSE) MyLogger.info(s"MainIndexController.index")       
      Ok(views.html.mainIndex(M2_VERSION, userName))
    }
  } 
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MainIndexController.scala
//=============================================================================