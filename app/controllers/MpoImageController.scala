/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers
//=============================================================================
import akka.util.ByteString
import play.api.data._
import play.api.Configuration
import play.api.http.HttpEntity
import javax.inject.Inject
import play.api.mvc._
//=============================================================================
import forms.MpoInfoForm._
import logger.MyLogger
import api.m2_api.M2_Api
import api.m2_api.M2_WebAstrometryResult
import controllers.ControllerCommon._
import version.Version.M2_VERSION
import models.UserModel
import api.m2_api.M2_Path
//=============================================================================
//It is linked with the related html class in 'views' directory
class MpoImageController @Inject()
  (cc: MessagesControllerComponents 
  , conf: Configuration) 
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private val postUrl = routes.MpoImageController.filter()
  //---------------------------------------------------------------------------
  private var remoteIP = ""
  //---------------------------------------------------------------------------
  private val m2_api = M2_Api(conf)
  private var m2_result: M2_WebAstrometryResult = null
  //---------------------------------------------------------------------------
  private var mpoComposedName = ""
  //---------------------------------------------------------------------------
  def index(_mpoComposedName:String, userName: String) = Action { implicit request: MessagesRequest[AnyContent] =>     
   UserLoginController.withValidatedUser { userRequest => 
      mpoComposedName = _mpoComposedName    
      remoteIP = request.remoteAddress.toString()
      val spkID = mpoComposedName.split("_")(1)
      if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoImageController.index($spkID)")   
      val m2_path = M2_Path(conf,mpoComposedName)
      m2_result = m2_api.astrometry(spkID, userName, remoteIP, m2_path).get          
      Ok(views.html.mpoImage(form, postUrl, m2_result, M2_VERSION))
   }
  }
  //---------------------------------------------------------------------------
  def reset() = Action { implicit request: MessagesRequest[AnyContent] =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoImageController.reset")   
    m2_result.reset() 
    Ok(views.html.mpoImage(form, postUrl, m2_result, M2_VERSION))
  }    
  //---------------------------------------------------------------------------
   def downloadImageSeq() =  withPlayUser { user =>

    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoImageController.downloadImageSeq")    

    downloadFile(m2_result.downladAndCompressFileSeq()  
                , s"${mpoComposedName}_image_seq.zip"
                , "zip")          
  }
  //---------------------------------------------------------------------------
  def downloadZipImage() = withPlayUser { user =>
     if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoImageController.downloadZipImage: '${m2_result.genericParameter}'")  
     downloadFile(m2_result.genericParameter
                  , s"${mpoComposedName}_image_seq.zip"
                  , "zip")
  }
  //---------------------------------------------------------------------------
  def downloadCsv() = withPlayUser { user =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoImageController.downloadCsv")    
    val csvContent = ByteString(M2_WebAstrometryResult.CSV_HEADER + "\n" + 
                                m2_result.getCsvContent().mkString("\n"))  
    val downloadedFilename = s"${mpoComposedName}_images.csv"
    val headers = Map(
      CONTENT_DISPOSITION -> s"attachment; filename=$downloadedFilename"
    )                                    
    Result(
      header = ResponseHeader(OK, headers),
      body = HttpEntity.Strict(csvContent, Some("text/csv"))
    )
  }
  //---------------------------------------------------------------------------
  // This will be the action that handles our form post
  def filter = Action { implicit request: MessagesRequest[AnyContent] => 

     UserLoginController.withValidatedUser { userRequest => 
       val errorFunction = { formWithErrors: Form[UserData] =>
        // This is the bad case, where the form had validation errors.
        // Let's show the user the form again, with the errors highlighted.
        // Note how we pass the form with errors to the template.
        BadRequest(views.html.mpoImage(formWithErrors, postUrl, M2_WebAstrometryResult(), M2_VERSION))
      }

      val successFunction = { userFilterData: UserData =>    
        if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoImageController.filter(${userFilterData})")    
        m2_result.applyFilter(userFilterData)
        Ok(views.html.mpoImage(form, postUrl, m2_result, M2_VERSION))    
      }

      val formValidationResult = form.bindFromRequest()

      formValidationResult.fold(errorFunction, successFunction)
    }
  }
  //---------------------------------------------------------------------------
  private def withPlayUser[T](block: UserModel => Result): EssentialAction = 
    Security.WithAuthentication(UserLoginController.extractUser)(user => Action(block(user)))  
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoImageController.scala
//=============================================================================