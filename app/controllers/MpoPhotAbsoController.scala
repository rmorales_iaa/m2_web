/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers
//=============================================================================
import javax.inject.Inject
import play.api.mvc._
import play.api.Configuration
import sys.process._
//=============================================================================
import logger.MyLogger
import controllers.ControllerCommon._
import api.m2_api.M2_Path
import version.Version.M2_VERSION
import controllers.helpers.ActionWithValidUser
import java.nio.file.{Files,Paths}
//=============================================================================
//=============================================================================
//It is linked with the related html class in 'views' directory
class MpoPhotAbsoController @Inject()
  (implicit cc: MessagesControllerComponents
           , conf: Configuration
           , actionWithValidUser: ActionWithValidUser) 
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private var m2_path: M2_Path = null
  //---------------------------------------------------------------------------
  private var remoteIP = ""
  private var userName = ""
  //---------------------------------------------------------------------------
  private val pngOutputPath = {
    val path = s"$currentDirectory/public/images/"
    ensureDirectoryExist(path)
    path
  }
  //---------------------------------------------------------------------------
  //plots
  private val M2_COMMNAD_GENERATE_PNG_COMMAND = conf.get[String]("m2.plot.generate.png.command")
  //---------------------------------------------------------------------------
  private def generateImagesFromPlot(mpoName: String) = {    
     
    if (!fileExist(m2_path.M2_RESULT_PHOT_ABSO_GNUPLOT_SCRIPT_NAME_SEQ.last._1)) Map[String,String]()
    else {      
        m2_path.M2_RESULT_PHOT_ABSO_GNUPLOT_SCRIPT_NAME_SEQ.zipWithIndex.map { case ((gnuPlotScript,isRotPhaseGnuPlotScript),i) =>

        val fixedGnuPlotScript = gnuPlotScript + "_fixed"
      
        if (isRotPhaseGnuPlotScript) fixGnuRotPhaseScript(gnuPlotScript, fixedGnuPlotScript)
        else fixGnuPlotScript(gnuPlotScript, fixedGnuPlotScript)

        val outputPngFileName = s"$pngOutputPath/phot_abso_${i}_${mpoName}.png"            
        val m2_command  = M2_COMMNAD_GENERATE_PNG_COMMAND
                         .replace("$1", outputPngFileName)
                         .replace("$2", fixedGnuPlotScript)         
        if (VERBOSE) MyLogger.info(m2_command)    
        val processBuilder = Process(Seq("bash", "-c", s"$m2_command"))       
        processBuilder.!     
        Files.delete(Paths.get(fixedGnuPlotScript))
        i.toString -> ("images/" + Paths.get(outputPngFileName).getFileName().toString())
      }.toMap   
    }    
  }  
  //--------------------------------------------------------------------------- 
  def index(mpoName: String, _userName:String) = Action { implicit request: MessagesRequest[AnyContent] => 
    UserLoginController.withValidatedUser { userRequest => 
      userName = _userName
      remoteIP = request.remoteAddress.toString()
      if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoPhotAbsoController.index")   
      m2_path = M2_Path(conf,mpoName)
      val pngMap = generateImagesFromPlot(mpoName)
      if (pngMap.isEmpty)        
        Redirect(routes.ErrorMessage.showError(s"There is no absolute photometry data for:'$mpoName'"))
      else 
        Ok(views.html.mpoPhotAbso(mpoName,pngMap,M2_VERSION))
    }    
  }    
  //---------------------------------------------------------------------------
  def downloadData(mpoName: String) = actionWithValidUser { userRequest =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoPhotAbsoController.downloadData")        
    val baseDir = generateUniqueDirectoryName(s"$TMP_DIR/${userName}_${remoteIP}_")
    val compressedFileName = s"${mpoName}_phot_abso.zip"
    val compressedFile = compressDirectoryWithZip(m2_path.M2_RESULT_PHOT_ABSO_COMPRESS_PATH
                                                  , s"$baseDir/$compressedFileName")                                                  
    downloadFile(compressedFile
                , compressedFileName
                , "zip")            
  }    
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoPhotAbsoController.scala
//=============================================================================