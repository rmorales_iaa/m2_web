/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers
//=============================================================================
import javax.inject.Inject
import play.api.data._
import play.api.mvc._
import play.api.Configuration
//=============================================================================
import forms.MpoSearchForm._
import logger.MyLogger
import api.m2_api.M2_Api
import controllers.ControllerCommon._
import controllers.MpoSearchControllerStatusMap
import api.m2_api.M2_Path
import version.Version.M2_VERSION
import controllers.helpers.ActionWithValidUser
import activityRecorder.ActivityRecorder
//=============================================================================
//=============================================================================
//It is linked with the related html class in 'views' directory
class MpoSearchController @Inject()
  (implicit cc: MessagesControllerComponents
  , conf: Configuration
  , actionWithValidUser: ActionWithValidUser) 
    extends MessagesAbstractController(cc) {

  //---------------------------------------------------------------------------
  private val statusMap = MpoSearchControllerStatusMap()
  //---------------------------------------------------------------------------
  private val m2_api = M2_Api(conf)
  private var m2_path: M2_Path = null
  //---------------------------------------------------------------------------
  private var mpoName = ""
  private var mpoComposedName = ""
  //---------------------------------------------------------------------------
  private val postUrl = routes.MpoSearchController.search()
  //---------------------------------------------------------------------------
  private var remoteIP = ""
  private var userName = ""
  //---------------------------------------------------------------------------
  def index(_userName: String) = {
     userName = _userName
     search
  }   
  //---------------------------------------------------------------------------
  def images = actionWithValidUser { user =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoSearchController.images")  
    Redirect(routes.MpoImageController.index(mpoComposedName,userName))   
  }  
  //---------------------------------------------------------------------------
  def astrometry = actionWithValidUser { user =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoSearchController.astrometry")   

    val baseDir = generateUniqueDirectoryName(s"$TMP_DIR/${userName}_${remoteIP}_")
    val compressedFileName = s"${mpoComposedName}_astrometry.zip"
    val inputDir = m2_path.M2_RESULT_COMPILED_COMPRESS_PATH
    if (getSortedFileListRecursive(inputDir).isEmpty)
      Redirect(routes.ErrorMessage.showError(s"There is no compiled data for:'${mpoComposedName}'"))
    else {
      val compressedFile = compressDirectoryWithZip(inputDir
                                                  , s"$baseDir/$compressedFileName")                                                  

      downloadFile(compressedFile
                   , compressedFileName
                  , "zip")         
    }     
  }
  //---------------------------------------------------------------------------
  def absoPhot = actionWithValidUser { user =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoSearchController.photAbso")   
    Redirect(routes.MpoPhotAbsoController.index(mpoComposedName,userName)) 
  }
  //---------------------------------------------------------------------------
  def diffPhot = actionWithValidUser { user =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoSearchController.photDiff")   
    Redirect(routes.MpoPhotDiffController.index(mpoComposedName,userName)) 
  }
   //---------------------------------------------------------------------------
  def vr = actionWithValidUser { user =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoSearchController.vr")   
    Redirect(routes.MpoVR_Controller.index(mpoComposedName,userName))     
  }  
  //---------------------------------------------------------------------------
  // This will be the action that handles our form post
  def search = Action { implicit request: MessagesRequest[AnyContent] => 

     UserLoginController.withValidatedUser { userRequest => 

       remoteIP = request.remoteAddress

       val errorFunction = { formWithErrors: Form[UserData] =>
        // This is the bad case, where the form had validation errors.
        // Let's show the user the form again, with the errors highlighted.
        // Note how we pass the form with errors to the template.
        BadRequest(views.html.mpoSearch(formWithErrors, postUrl,statusMap,M2_VERSION))
       }

      val successFunction = { userData: UserData =>      
        if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoSearchController.search(${userData.name})")   
        mpoName = userData.name.trim.replaceAll(" ","_")      
        if (mpoName.trim.isEmpty()) statusMap.resetErrorMessage()              
        else {
          val r = m2_api.exists(conf, mpoName, userName, remoteIP)     
          if (r.isDefined) {                   
            mpoComposedName = r.get
            if (VERBOSE) MyLogger.info(s"$remoteIP Processing mpo:'$mpoComposedName'")
            m2_path = M2_Path(conf,mpoComposedName)

            statusMap.showLinkSeq(m2_path)                     
            statusMap.resetErrorMessage()          
            statusMap.setMpoName(mpoName)
          }
          else {          
            mpoComposedName = mpoName
            statusMap.resetAllLinkSeq()          
            statusMap.setMpoName(mpoName)

            //try to get the V-R
            m2_path = M2_Path(conf,mpoComposedName)
            val vrTmpDir = m2_path.M2_RESULT_M2_WEB_PATH
            val vrResultFile = m2_api.vr(mpoName, userName, remoteIP, m2_path)

            if (!vrResultFile.isEmpty) {
              statusMap.enableVR(vrTmpDir)
              statusMap.setErrorMessage(s"Can not find mpo: '$mpoName' in the SPICE database but found in compiled V-R values")                        
            }
            else 
              statusMap.setErrorMessage(s"Can not find mpo: '$mpoName' in the SPICE database neither in compiled V-R values")                        
          }          
        }

        ActivityRecorder(userName
                        , remoteIP                         
                        , "mpo search"
                        , mpoName)

        Ok(views.html.mpoSearch(form, postUrl, statusMap, M2_VERSION))         
      }
    
      val formValidationResult = form.bindFromRequest()

      formValidationResult.fold(errorFunction, successFunction)
    }    
  } 
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoSearchController.scala
//=============================================================================