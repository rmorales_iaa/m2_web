/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers
//=============================================================================
//=============================================================================
object MpoSearchControllerStatus {
  //---------------------------------------------------------------------------
  def apply() : MpoSearchControllerStatus = {    
    MpoSearchControllerStatus(resetStatusValueMap())
  }
  //---------------------------------------------------------------------------
  def resetStatusValueMap() = 
    scala.collection.mutable.Map(
        "link_images"      -> ("",false)
      , "link_astrometry"  -> ("",false)
      , "link_abso_phot"   -> ("",false)
      , "link_diff_phot"   -> ("",false)      
      , "link_v_r"         -> ("",false)      
      , "error_message"    -> ("",false)
    )  
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MpoSearchControllerStatus(
  statusMap: scala.collection.mutable.Map[String,(String,Boolean)]) {
   //---------------------------------------------------------------------------
  def resetErrorMessage() = 
    statusMap("error_message")  = ("", false)
  //---------------------------------------------------------------------------
  def setErrorMessage(message: String) = 
    statusMap("error_message")  = (message, true)
  //---------------------------------------------------------------------------  
  def showAllLinkSeq(): Unit = {
    statusMap.foreach { case (key, _) =>
      statusMap(key) = ("", false)
    }
    resetErrorMessage()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoSearchControllerStatus.scala
//=============================================================================