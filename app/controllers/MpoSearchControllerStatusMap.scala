/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers

import api.m2_api.M2_Path
//=============================================================================
//=============================================================================
object MpoSearchControllerStatusMap {
  //---------------------------------------------------------------------------
  def apply() : MpoSearchControllerStatusMap =
    MpoSearchControllerStatusMap( scala.collection.mutable.Map(
        "link_images"      -> ("",false)
      , "link_astrometry"  -> ("",false)
      , "link_abso_phot"   -> ("",false)
      , "link_diff_phot"   -> ("",false)      
      , "link_v_r"         -> ("",false)      
      , "mpo_name"         -> ("",false)
      , "error_message"    -> ("",false)
    ) )    
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MpoSearchControllerStatusMap(
  map: scala.collection.mutable.Map[String,(String,Boolean)]
 ) {
  //---------------------------------------------------------------------------
  def setMpoName(name: String) = 
    map("mpo_name") = (name, true)
  //---------------------------------------------------------------------------
  def resetMpoName() = 
    map("mpo_name") = ("", false)
  //---------------------------------------------------------------------------
  def setErrorMessage(message: String) = 
    map("error_message") = (message, true)
  //---------------------------------------------------------------------------
  def resetErrorMessage() = 
    map("error_message") = ("", false)  
  //---------------------------------------------------------------------------  
  def showLinkSeq(m2_path: M2_Path): Unit = {
    map.foreach { case (key, _) => map(key) = (key, true) }    
    map("link_astrometry") = ("",ControllerCommon.directoryExist(m2_path.M2_RESULT_COMPILED_COMPRESS_PATH))
    map("link_abso_phot")  = ("",ControllerCommon.directoryExist(m2_path.M2_RESULT_PHOT_ABSO_COMPRESS_PATH))
    map("link_diff_phot")  = ("",ControllerCommon.directoryExist(m2_path.M2_RESULT_PHOT_DIFF_COMPRESS_PATH))
    map("link_v_r")        = ("",ControllerCommon.directoryExist(m2_path.M2_RESULT_M2_WEB_PATH))
  }
  //---------------------------------------------------------------------------  
  def resetAllLinkSeq(): Unit = 
    map.foreach { case (key, _) => map(key) = ("", false) }    
    //---------------------------------------------------------------------------  
  def enableVR(path:String): Unit = map("link_v_r") = (path,true)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoSearchControllerStatusMap.scala
//=============================================================================