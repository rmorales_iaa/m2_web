/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers
//=============================================================================
import javax.inject.Inject
import play.api.mvc._
import play.api.Configuration
//=============================================================================
import logger.MyLogger
import controllers.ControllerCommon._
import version.Version.M2_VERSION
import controllers.helpers.ActionWithValidUser
import api.m2_api.M2_Api
import api.m2_api.M2_Path
//=============================================================================
//=============================================================================
//It is linked with the related html class in 'views' directory
class MpoVR_Controller @Inject()
  (implicit cc: MessagesControllerComponents
           , conf: Configuration
           , actionWithValidUser: ActionWithValidUser) 
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private val m2_api = M2_Api(conf)
  private var m2_path: M2_Path = null
  //---------------------------------------------------------------------------
  private var remoteIP = ""
  private var userName = ""
  //--------------------------------------------------------------------------- 
  def index(_mpoName: String, _userName:String) = Action { implicit request: MessagesRequest[AnyContent] => 
    UserLoginController.withValidatedUser { userRequest => 
      userName = _userName
      remoteIP = request.remoteAddress.toString()
      m2_path = M2_Path(conf,_mpoName.replaceAll(" ","_"))

      val  mpoName = _mpoName.replaceAll(" ","_")

      if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoVR_Controller.index")   
    
      val vrResultFile = m2_api.vr(mpoName, userName, remoteIP, m2_path)
      if (vrResultFile.isEmpty) 
         Ok(views.html.mainIndex(M2_VERSION, userName))
      else {
        val source = scala.io.Source.fromFile(vrResultFile.get)
        val lineSequence = try source.mkString finally source.close()
        val rowSeq = lineSequence.split("\n").map (_.split(",")).drop(1)
        Ok(views.html.mpoVR(mpoName
                            , m2_path.M2_RESULT_M2_WEB_PATH
                            , rowSeq
                            , M2_VERSION))
      }
    }        
  }     
  //---------------------------------------------------------------------------
  def downloadData(mpoName: String, vrResultFileDir: String) = actionWithValidUser { userRequest =>
    if (VERBOSE) MyLogger.info(s"$remoteIP -----> MpoVR_Controller.downloadData")        
    val baseDir = generateUniqueDirectoryName(s"$TMP_DIR/${userName}_${remoteIP}_")
    val compressedFileName = s"${mpoName}_vr.zip"
    val compressedFile = compressDirectoryWithZip(vrResultFileDir
                                                  , s"$baseDir/$compressedFileName")                                                  
    downloadFile(compressedFile
                , compressedFileName
                , "zip")            
  }    
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoVR_Controller.scala
//=============================================================================