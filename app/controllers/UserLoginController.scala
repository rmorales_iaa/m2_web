/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetController.scala
  https://www.theguardian.com/info/developer-blog/2015/dec/30/how-to-add-a-form-to-a-play-application
 */
//=============================================================================
package controllers
//=============================================================================
import javax.inject.Inject
import play.api.data._
import play.api.mvc._
import play.api.Configuration
import java.time.LocalDateTime
import java.time.ZoneOffset
import play.api.mvc.Results._
//=============================================================================
import forms.UserLoginForm._
import logger.MyLogger
import controllers.ControllerCommon._
import version.Version.M2_VERSION
import models.UserModel
import models.helpers.Session
import activityRecorder.ActivityRecorder
//=============================================================================
//=============================================================================
object UserLoginController{
  //---------------------------------------------------------------------------
  def extractUser(req: RequestHeader): Option[UserModel] = {
    val sessionTokenOpt = req.session.get("sessionToken")
    sessionTokenOpt
      .flatMap(token => Session.getSession(token))
        .filter(_.expiration.isAfter(LocalDateTime.now(ZoneOffset.UTC)))
        .map(_.userName)
        .flatMap(UserModel.getUser)
  }
  //---------------------------------------------------------------------------
  def withValidatedUser[T](block: UserModel => Result)
    (implicit request: Request[AnyContent]): Result = {
    val user = extractUser(request)
    user
      .map(block)
      .getOrElse(Unauthorized(views.html.defaultpages.unauthorized())) // 401, but 404 could be better from a security point of view
  }   
  //---------------------------------------------------------------------------
}
//=============================================================================
class UserLoginController @Inject()
  (cc: MessagesControllerComponents
  , conf: Configuration) 
    extends MessagesAbstractController(cc) {
  //---------------------------------------------------------------------------
  private val postUrl = routes.UserLoginController.login()
  //---------------------------------------------------------------------------
  private var remoteIP = ""
  //---------------------------------------------------------------------------    
  def index() = Action { implicit request: MessagesRequest[AnyContent] =>
    UserModel.fillValidatedUserMap(conf)
    Ok(views.html.index(form, postUrl, M2_VERSION))
  }
  //---------------------------------------------------------------------------
  // This will be the action that handles our form post
  def login = Action { implicit request: MessagesRequest[AnyContent] =>

    remoteIP = request.remoteAddress

    val errorFunction = { formWithErrors: Form[UserData] =>  
      BadRequest(views.html.index(formWithErrors, postUrl,M2_VERSION))
    }

    val successFunction = { userData: UserData =>      
      val userName = userData.name.trim
      val password = userData.password.trim
      if (VERBOSE) MyLogger.info(s"$remoteIP -----> UserLoginController.login(${userName})")   
    
      if (isValidLogin(userName, password)) {

        ActivityRecorder(userName
                        , remoteIP                         
                        , "login"
                        , "")
 
        val token = Session.generateToken(userName)        
        Redirect(routes.MainIndexController.index(userName))
          .withSession(request.session + ("sessionToken" -> token))        
      } 
      else 
        Unauthorized(views.html.defaultpages.unauthorized()).withNewSession        
    }
    
    val formValidationResult = form.bindFromRequest()

    formValidationResult.fold(errorFunction, successFunction)
  } 
  //---------------------------------------------------------------------------
  private def isValidLogin(userName: String, password: String): Boolean =
    UserModel.getUser(userName).exists(_.password == password)       
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file UserLoginController.scala
//=============================================================================