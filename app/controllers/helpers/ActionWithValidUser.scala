/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: adapted from https://pedrorijo.com/blog/scala-play-auth/
 */
//=============================================================================
package controllers.helpers
//=============================================================================
import java.time.{LocalDateTime, ZoneOffset}
import javax.inject.Inject
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import models.UserModel
import models.helpers.Session
//=============================================================================
//=============================================================================
class UserRequest[A](val user: Option[UserModel]
                      , request: Request[A]) extends WrappedRequest[A](request)
//=============================================================================
class ActionWithValidUser @Inject()(val parser: BodyParsers.Default)
(implicit val executionContext: ExecutionContext)
  extends ActionBuilder[UserRequest, AnyContent]
    with ActionTransformer[Request, UserRequest] {
  //--------------------------------------------------------------------------- 
  def transform[A](request: Request[A]) = Future.successful {

    val sessionTokenOpt = request.session.get("sessionToken")

    val user = sessionTokenOpt
      .flatMap(token => Session.getSession(token))
      .filter(_.expiration.isAfter(LocalDateTime.now(ZoneOffset.UTC)))
      .map(_.userName)
      .flatMap(UserModel.getUser)

    new UserRequest(user, request)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================