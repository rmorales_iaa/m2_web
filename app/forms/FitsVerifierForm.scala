//=============================================================================
/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetForm.scala
 */
//=============================================================================
package forms
//=============================================================================
import play.api.data.Forms._
import play.api.data.Form
//=============================================================================
//=============================================================================
object FitsVerifierForm{
  //--------------------------------------------------------------------------- 
  case class UserData(name: String) 
  //---------------------------------------------------------------------------
  val form = Form(
    mapping(
        "path" -> default(text, ""))(UserData.apply)(UserData.unapply)
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsVerifierForm.scala
//=============================================================================