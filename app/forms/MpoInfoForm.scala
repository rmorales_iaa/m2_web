//=============================================================================
/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
  https://www.playframework.com/documentation/2.8.x/ScalaForms
  https://pedrorijo.com/blog/advanced-play-forms/
  https://github.com/playframework/play-samples/blob/2.8.x/play-scala-forms-example/app/controllers/WidgetForm.scala
 */
//=============================================================================
package forms
//=============================================================================
import play.api.data.Forms._
import play.api.data.Form
//=============================================================================
import controllers.ControllerCommon._
//=============================================================================
//=============================================================================
object MpoInfoForm {
  //--------------------------------------------------------------------------- 
  /**
   * A form processing DTO that maps to the form below.
   *
   * Using a class specifically for form binding reduces the chances
   * of a parameter tampering attack and makes code clearer.
   */
  //It is linked with the related controller class
  case class UserData(telescope         : String
                      , filter          : String
                      , instrument      : String
                      , expTime         : String 
                      , customObsMinTime: String
                      , customObsMaxTime: String) 
  //---------------------------------------------------------------------------
  /**
   * The form definition for the "create a widget" form.
   * It specifies the form fields and their types,
   * as well as how to convert from a Data to form data and vice versa.
   */
  //It is linked with the related controller html form class in 'views' directory
  //In the html, use the name in the mappings for the defined objects

  val form = Form(
    mapping(
          "list_telescope"             -> default(text, ANY_USER_VALUE)
        , "list_image_filter"          -> default(text, ANY_USER_VALUE)
        , "list_instrument"            -> default(text, ANY_USER_VALUE)
        , "list_exp_time_s"            -> default(text, ANY_USER_VALUE)
        , "input_custom_obs_min_time"  -> default(text, ANY_USER_VALUE)
        , "input_custom_obs_max_time"  -> default(text, ANY_USER_VALUE)
    )
    (UserData.apply)(UserData.unapply)
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoInfoForm.scala
//=============================================================================