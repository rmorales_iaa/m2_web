/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
    https://www.playframework.com/documentation/2.8.x/ScalaForms
    https://pedrorijo.com/blog/advanced-play-forms/
 */
package models
//=============================================================================
//=============================================================================
//It is linked with the UserData class in the related form
case class MpoInfoModel(telescope         : String
                        , filter          : String
                        , instrument      : String
                        , expTime         : String 
                        , customObsMinTime: String
                        , customObsMaxTime: String)    
                        
//=============================================================================
//End of file MpoInfoModel.scala
//=============================================================================