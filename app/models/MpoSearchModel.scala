/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: 
    https://www.playframework.com/documentation/2.8.x/ScalaForms
    https://pedrorijo.com/blog/advanced-play-forms/
 */
package models
//=============================================================================
//=============================================================================
//It is linked with the UserData class in the related form
case class MpoSearchModel(name: String)    
//=============================================================================
//End of file MpoSearchModel.scala
//=============================================================================