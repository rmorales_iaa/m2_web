/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: adapted from https://pedrorijo.com/blog/scala-play-auth/
 */
package models
//=============================================================================
import play.api.Configuration
//=============================================================================
object UserModel {
  //---------------------------------------------------------------------------
  // Map username -> User
  private val validatedUserMap = scala.collection.mutable.Map[String, UserModel]()
  //---------------------------------------------------------------------------
  def fillValidatedUserMap(conf: Configuration)  = {
    if (validatedUserMap.isEmpty) {
      synchronized {
        val userNameSeq = conf.get[String]("m2_web.user.seq").split(";")
        val passwordSeq = conf.get[String]("m2_web.pass.seq").split(";")
        (userNameSeq zip passwordSeq).foreach { case (username,password) => 
          addUser(username.trim,password.trim)
        }
      }
    }   
  }
  //---------------------------------------------------------------------------
  def getUser(username: String): Option[UserModel] =  validatedUserMap.get(username)  
  //---------------------------------------------------------------------------
  def addUser(username: String, password: String): Option[UserModel] =     
    synchronized {
      if(validatedUserMap.contains(username)) Option.empty
      else {
        val user = UserModel(username, password)
        validatedUserMap.put(username, user)
        Option(user)     
      }    
    }  
  //---------------------------------------------------------------------------
}
//=============================================================================
case class UserModel(username: String, password: String)
//=============================================================================
//End of file UserModel.scala
//=============================================================================