/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Aug/2023 
 * Time:  21h:20m
 * Description: adapted from https://pedrorijo.com/blog/scala-play-auth/
 */
package models.helpers
//=============================================================================
import java.util.UUID
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
object Session {    
  //---------------------------------------------------------------------------
  // Map token -> Session
  private val sessions= scala.collection.mutable.Map.empty[String, Session]
  //---------------------------------------------------------------------------
  def getSession(token: String): Option[Session] = sessions.get(token)

  //---------------------------------------------------------------------------
  def generateToken(username: String): String = {
    // we use UUID to make sure randomness and uniqueness on tokens  
    val token = s"$username-token-${UUID.randomUUID().toString}"
    sessions.put(token, Session(token, username, LocalDateTime.now().plusHours(6)))
    token
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Session(token: String
                   , userName: String
                   , expiration: LocalDateTime)
//=============================================================================
//End of file Sesion.scala
//=============================================================================