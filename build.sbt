//-----------------------------------------------------------------------------
lazy val _scalaVersion      = "2.13.11"
lazy val programName        = "m2_web"
lazy val programVersion     = "0.0.1"
lazy val programDescription = "m2 and database web access"
lazy val authorList         = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val authorOrganization = "www.iaa.es"
lazy val license            = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
lazy val _homepage          = "http://www.iaa.csic.es"
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := _scalaVersion
  , description        := programDescription
  , organization       := authorOrganization
  , homepage           := Some(url(_homepage))  
)
//-----------------------------------------------------------------------------
 maintainer := "rmorales@iaa.es"
//-----------------------------------------------------------------------------
//dependences versions

val akkaHttpServerVersion    = "2.9.0-M6"
val akkaSystemVersion        = "2.8.3"

val apacheCsvVersion         = "1.10.0"

val mongoScalaDriverVersion  = "4.10.2"

val playLoggerVersion        = "2.9.0-M6"
val playTestVersion          = "6.0.0-M6"
val playJansiTerminalVersion = "3.23.0"

val reactiveMongoPlayVersion = "1.1.0-play28-RC11"  
val reactiveMongoOptimizationVersion = "1.1.0-RC6-linux-x86-64"

val twirlCompilerVersion     = "1.6.0-RC4"
//please, rememeber to add in plugin.sbt addSbtPlugin("com.typesafe.play" % "sbt-twirl" % "1.6.0-RC4")
//and  enable it: .enablePlugins(SbtTwirl)

//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//https://pedrorijo.com/blog/scala-compiler-review-code-warnings/
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq (
  "-Xlint"
  , "-deprecation"
  , "-feature"
  , "-unchecked"
  , "-Ywarn-dead-code"
  , "-Yrangepos"  //required for semanticdb
 // , "-Xfatal-warnings" 
)
//-----------------------------------------------------------------------------
//resolvers
resolvers ++= Seq(
    "Typesafe Repo" at "https://repo.typesafe.com/typesafe/releases/"
  , "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases/"
  , "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"
)
//-----------------------------------------------------------------------------
//remote debugging : https://www.scala-sbt.org/1.x/docs/IDE.html
ThisBuild / bspEnabled := false
//-----------------------------------------------------------------------------
//operating system
val os = sys.props.get("os.name") match {
  case Some(osx) if osx.toLowerCase.startsWith("mac") => "osx"
  case _ => "linux"
}
//-----------------------------------------------------------------------------
//inject depenedeces (as path)
routesGenerator := InjectedRoutesGenerator
//-----------------------------------------------------------------------------
//bson types used as bindings of the Play routing
import play.sbt.routes.RoutesKeys
RoutesKeys.routesImport += "play.modules.reactivemongo.PathBindables._"
//-----------------------------------------------------------------------------
//dependencies 
lazy val dependenceList = Seq(

  //lightweight dependency injection framework 
  guice
         
  //scala reflect
  , "org.scala-lang" % "scala-reflect" % _scalaVersion
  
  //logging
  , "com.typesafe.play" %% "play-logback" % playLoggerVersion

  //reactive mongo for Play 2.8
  , "org.reactivemongo" %% "play2-reactivemongo" % reactiveMongoPlayVersion

  // JSON serialization for reactive mongo
  , "org.reactivemongo" %% "reactivemongo-play-json-compat" % reactiveMongoPlayVersion

  //netty  native
  , "org.reactivemongo" % "reactivemongo-shaded-native" % reactiveMongoOptimizationVersion % "runtime"
  
  //apache csv
  ,  "org.apache.commons" % "commons-csv" % apacheCsvVersion

  // akka http server
  , "com.typesafe.play" %% "play-akka-http-server" % akkaHttpServerVersion   

  //akka system
  , "com.typesafe.akka" %% "akka-actor" % akkaSystemVersion
  , "com.typesafe.akka" %% "akka-serialization-jackson" % akkaSystemVersion
  , "com.typesafe.akka" %% "akka-slf4j" % akkaSystemVersion
  , "com.typesafe.akka" %% "akka-stream" % akkaSystemVersion
  , "com.typesafe.akka" %% "akka-protobuf-v3" % akkaSystemVersion
  , "com.typesafe.akka" %% "akka-actor-typed" % akkaSystemVersion

   //twirl compiler
  , "com.typesafe.play" %% "twirl-compiler" % twirlCompilerVersion
  , "com.typesafe.play" %% "twirl-api" % twirlCompilerVersion

  //jansi terminal
  , "org.jline" % "jline-terminal-jansi" % playJansiTerminalVersion
  
  //test
  , "org.scalatestplus.play" %% "scalatestplus-play" % playTestVersion  % Test
)
//=============================================================================
//to avoid reloading when a long task is runnung  
PlayKeys.devSettings := Seq( "play.server.http.idleTimeout" -> "15 m")
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(PlayScala, SbtTwirl)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'process.sbt'
//-----------------------------------------------------------------------------
