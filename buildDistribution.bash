#!/bin/bash
#-----------------------------------------------------------------------
ZIP=/home/rafa/proyecto/m2_web/target/universal/m2_web-0.0.1.zip
DEPLOY_DIR=./deploy
#------------------------------------------------------------------------

rm -fr $ZIP

sbt dist

if [ ! -d "$DEPLOY_DIR" ]; then
  mkdir -p "$DEPLOY_DIR"
  echo "Directory created: $DEPLOY_DIR"
else
  echo "Using directory: $DEPLOY_DIR"
fi

cp $ZIP deploy
cd deploy
rm -fr  m2_web-0.0.1
unzip m2_web-0.0.1.zip
rm m2_web-0.0.1.zip
cd ..
#------------------------------------------------------------------------
