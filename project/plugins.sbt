//-----------------------------------------------------------------------------
//start
//-----------------------------------------------------------------------------

//solve dependences in sbt > 1.4.9 for scala-xml
ThisBuild / libraryDependencySchemes ++= Seq(
  "org.scala-lang.modules" %% "scala-xml" % VersionScheme.Always
)

//https://index.scala-lang.org/playframework/playframework/badges
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.20")


addSbtPlugin("org.foundweekends.giter8" % "sbt-giter8-scaffold" % "0.11.0")

//dependence graph
addDependencyTreePlugin

//https://github.com/playframework/twirl
addSbtPlugin("com.typesafe.play" % "sbt-twirl" % "1.6.0-RC4")

//https://scalameta.org/docs/semanticdb/guide.html
addCompilerPlugin("org.scalameta" % "semanticdb-scalac" % "4.8.7" cross CrossVersion.full)

//-----------------------------------------------------------------------------
//end
//-----------------------------------------------------------------------------
