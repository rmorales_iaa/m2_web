#!/bin/bash

# Define the directories to clean
DIR_0=/home/rafa/proyecto/m2_web/tmp
DIR_1=/home/rafa/proyecto/m2_web/logs
DIR_2=/home/rafa/proyecto/m2_web/target/global-logging
DIR_3=/home/rafa/proyecto/m2_web/target/task-temp-directory
DIR_4=/home/rafa/proyecto/m2_web/public/images/

directories=("$DIR_0/" "$DIR_1/" "$DIR_2/" "$DIR_3/" "$DIR_4/")

# Iterate over the directories and delete old files
for dir in "${directories[@]}"; do
  rm -fr $dir
  mkdir $dir
done
