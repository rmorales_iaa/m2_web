
//https://www.playframework.com/documentation/2.8.x/ScalaTestingWithScalaTest
//https://www.playframework.com/documentation/2.8.x/ScalaFunctionalTestingWithScalaTest
//=============================================================================
package test 
//=============================================================================
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc.Results
import play.api.test.Helpers._
import play.api.test.FakeRequest
//=============================================================================
class AstrometryControllerSpec extends PlaySpec with GuiceOneAppPerSuite with Results {

  //---------------------------------------------------------------------------  
  "AstrometryController" should {

    //-------------------------------------------------------------------------
    "return OK for POST /astrometry" in {      
      val request = FakeRequest(POST, "/objectName")
                    .withFormUrlEncodedBody("key" -> "vino")
      val response = route(app, request).get

      status(response) mustBe OK
    } 
    //-------------------------------------------------------------------------
    "return OK for GET /astrometry" in {      
      val request = FakeRequest(GET, "/astrometry")
      val response = route(app, request).get

      status(response) mustBe OK
    }        
    //-------------------------------------------------------------------------    
  }  
}
//=============================================================================
//End of file AstrometryControllerSpec.scala
//=============================================================================
