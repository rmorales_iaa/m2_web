//https://www.playframework.com/documentation/2.8.x/ScalaTestingWithScalaTest
//https://www.playframework.com/documentation/2.8.x/ScalaFunctionalTestingWithScalaTest
//=============================================================================
package test 
//=============================================================================
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc.Results
import play.api.test.Helpers._
import play.api.test.FakeRequest
//=============================================================================
class MpcControllerSpec extends PlaySpec with GuiceOneAppPerSuite with Results {

  //---------------------------------------------------------------------------  
  "MpcController" should {
    //-------------------------------------------------------------------------
    "return OK for GET /Mpc" in {      
      val request = FakeRequest(GET, "/mpc/1")
      val response = route(app, request).get

      status(response) mustBe OK
    }        
    //-------------------------------------------------------------------------    
  }  
}
//=============================================================================
//End of file MpcControllerSpec.scala
//=============================================================================